/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.werapan.databaseproject.model;

import com.werapan.databaseproject.dao.CustomerDao;
import com.werapan.databaseproject.dao.UserDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author werapan
 */
public class Reciept {

    private int id;
    private Date createdDate;
    private float total;
    private float cash;
    private int totalQty;
    private int userId;
    private int customerId;
    private User user;
    private Customer customer;
    private ArrayList<RecieptDetail> recieptDetails = new ArrayList<RecieptDetail>();

    public Reciept(int id, Date created_date, float total, float cash, int total_qty, int user_id, int customer_id) {
        this.id = id;
        this.createdDate = created_date;
        this.total = total;
        this.cash = cash;
        this.totalQty = total_qty;
        this.userId = user_id;
        this.customerId = customer_id;
    }

    public Reciept(Date created_date, float total, float cash, int total_qty, int user_id, int customer_id) {
        this.id = -1;
        this.createdDate = created_date;
        this.total = total;
        this.cash = cash;
        this.totalQty = total_qty;
        this.userId = user_id;
        this.customerId = customer_id;
    }

    public Reciept(float total, float cash, int total_qty, int user_id, int customer_id) {
        this.id = -1;
        this.createdDate = null;
        this.total = total;
        this.cash = cash;
        this.totalQty = total_qty;
        this.userId = user_id;
        this.customerId = customer_id;
    }

    public Reciept(float cash, int user_id, int customer_id) {
        this.id = -1;
        this.createdDate = null;
        this.total = 0;
        this.cash = cash;
        this.totalQty = 0;
        this.userId = user_id;
        this.customerId = customer_id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date created_date) {
        this.createdDate = created_date;
    }

    public float getTotal() {
        return total;
    }

    public void setTotal(float total) {
        this.total = total;
    }

    public float getCash() {
        return cash;
    }

    public void setCash(float cash) {
        this.cash = cash;
    }

    public int getTotalQty() {
        return totalQty;
    }

    public void setTotalQty(int total_qty) {
        this.totalQty = total_qty;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
        //ปรับให้ตรงกัน
        this.userId = user.getId();
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
        //ปรับให้ตรงกัน
        this.customerId = customer.getId();
    }

    public ArrayList<RecieptDetail> getRecieptDetails() {
        return recieptDetails;
    }

    public void setRecieptDetails(ArrayList recieptDetails) {
        this.recieptDetails = recieptDetails;
    }

    public Reciept() {
        this.id = -1;
        this.createdDate = null;
        this.total = 0;
        this.cash = 0;
        this.totalQty = 0;
        this.userId = 0;
        this.customerId = 0;
    }

    @Override
    public String toString() {
        return "Reciept{" + "id=" + id + ", createdDate=" + createdDate + ", total=" + total + ", cash=" + cash + ", totalQty=" + totalQty + ", userId=" + userId + ", customerId=" + customerId + ", user=" + user + ", customer=" + customer + ", recieptDetails=" + recieptDetails + '}';
    }

    public void addRecieptDetail(RecieptDetail recieptDetail) {
        recieptDetails.add(recieptDetail);
        calculateTotal();
    }

    public void addRecieptDetail(Product product, int qty) {
        RecieptDetail rd = new RecieptDetail(product.getId(), product.getName(), product.getPrice(), qty, qty * product.getPrice(), -1);
        recieptDetails.add(rd);
        calculateTotal();
    }

    public void removeRecieptDetail(RecieptDetail recieptDetail) {
        recieptDetails.remove(recieptDetail);
        calculateTotal();
    }

    public void calculateTotal() {
        int totalQty = 0;
        float total = 0.0f;
        for (RecieptDetail rd : recieptDetails) {
            total += rd.getTotalPrice();
            totalQty += rd.getQty();
        }
        this.totalQty = totalQty;
        this.total = total;
    }

    public static Reciept fromRS(ResultSet rs) {
        Reciept reciept = new Reciept();
        try {
            reciept.setId(rs.getInt("reciept_id"));
            reciept.setCreatedDate(rs.getTimestamp("created_date"));
            reciept.setTotal(rs.getFloat("total"));
            reciept.setCash(rs.getFloat("cash"));
            reciept.setTotalQty(rs.getInt("total_qty"));
            reciept.setUserId(rs.getInt("user_id"));
            reciept.setCustomerId(rs.getInt("customer_id"));
            //population
            CustomerDao customerDao = new CustomerDao();
            UserDao userDao = new UserDao();
            Customer customer = customerDao.get(reciept.getCustomerId());
            User user = userDao.get(reciept.getUserId());
            reciept.setCustomer(customer);
            reciept.setUser(user);

        } catch (SQLException ex) {
            Logger.getLogger(Reciept.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return reciept;
    }
}
