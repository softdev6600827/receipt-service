/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package test_service;

import com.werapan.databaseproject.model.Customer;
import com.werapan.databaseproject.service.CustomerService;

/**
 *
 * @author ส้มส้ม
 */
public class TestCustomerService {

    public static void main(String[] args) {
        CustomerService cs = new CustomerService();
        for (Customer customer : cs.getCustomers()) {
            System.out.println(customer);
        }

        System.out.println(cs.getByTel("0888868888"));
        Customer cus1 = new Customer("menoi", "0888868888");
        cs.addNew(cus1);
        for (Customer customer : cs.getCustomers()) {
            System.out.println(customer);
        }

        Customer delcus = cs.getByTel("0888868888");
        delcus.setTel("0888868820");
        cs.update(delcus);
        System.out.println("Customer After Update");
        for (Customer customer : cs.getCustomers()) {
            System.out.println(customer);
        }
        cs.delete(delcus);
        for (Customer customer : cs.getCustomers()) {
            System.out.println(customer);
        }

    }
}
